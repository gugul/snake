import pygame
import random
import time

pygame.init()

# Screen setup
screen_width = 720
screen_height = 720
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Snake")

fps = pygame.time.Clock()

background_img = pygame.image.load("graphics/Pixel Grass Background Fabric1.jpg").convert()
background_img = pygame.transform.scale(background_img, (720, 640))
apple_img = pygame.image.load("graphics/apple-2327.png").convert_alpha()
apple_img = pygame.transform.scale(apple_img, (20, 20))
apple_rect = apple_img.get_rect()
snake_head_img = pygame.image.load("graphics/snake_head1.png").convert_alpha()
snake_head_img = pygame.transform.scale(snake_head_img, (20, 20))
rotated_head = snake_head_img

# Snake start position
snake_position = [100, 240]

# Snake's start body
snake_body = [[100, 240], [80, 240], [60, 240], [40, 240]]

apple_spawn = True

direction = "RIGHT"
change_dir = direction

# Sound
game_over_snd = pygame.mixer.Sound("sounds/kl-peach-game-over-iii-142453.mp3")
apple_snd = pygame.mixer.Sound("sounds/retro-video-game-coin-pickup-38299.mp3")
level_up = pygame.mixer.Sound("sounds/video-game-powerup-38065.mp3")

# Apple position
apple_position = [random.randrange(0, screen_width, 20),
                  random.randrange(0, screen_height - 100, 20)]

# Score
score = 0
run = False
start = True
current_level = 0


def start_screen():
    global score, snake_body, snake_position, direction
    score = 0
    snake_body = [[100, 240], [80, 240], [60, 240], [40, 240]]
    snake_position = [100, 240]
    direction = "RIGHT"
    font = pygame.font.SysFont("Arial", 100)
    sm_font = pygame.font.SysFont("Arial", 50)
    start_text = font.render("SNAKE", True, "Red")
    start_text1 = sm_font.render("press SPACE to start", True, "Red")
    start1_rect = start_text1.get_rect(center=(screen_width / 2, screen_height / 2 + 100))
    start_rect = start_text.get_rect(center=(screen_width / 2, screen_height / 2))
    screen.blit(start_text, start_rect)
    screen.blit(start_text1, start1_rect)


def show_score():
    pygame.draw.rect(screen, "black", (screen_width / 2 - 100, 650, 200, 40))
    # pygame.draw.rect(screen, "black", (0, 640, screen_width, 40))
    score_font1 = pygame.font.SysFont("times new roman", 30)
    score_srf = score_font1.render(f"SCORE: {score}", True, "White")
    score_rect = score_srf.get_rect(center=(screen_width / 2, 680))
    screen.blit(score_srf, score_rect)


def game_over():
    global run, start
    game_over_font = pygame.font.SysFont("times new roman", 60)
    game_over_srf = game_over_font.render(f"GAME OVER", True, "black")
    game_over_rect = game_over_srf.get_rect(midbottom=(screen_width / 2, 200))
    screen.blit(game_over_srf, game_over_rect)
    game_over_snd.set_volume(0.2)
    game_over_snd.play()
    start = True

    pygame.display.flip()
    run = False


def level_up_sound():
    level_up.set_volume(0.2)
    level_up.play(0)


def screen_boundaries():
    if snake_position[0] < 0:
        game_over()
    if snake_position[0] > screen_width - 20:
        game_over()
    if snake_position[1] < 0:
        game_over()
    if snake_position[1] > screen_height - 100:
        game_over()


def game_leveling_speed():
    global current_level
    if score < 50:
        fps.tick(5)
    elif 50 <= score < 100:
        if current_level == 0:
            level_up_sound()
            current_level = 1
        fps.tick(10)
    elif 100 <= score < 150:
        fps.tick(15)
        if current_level == 1:
            level_up_sound()
            current_level = 2
    elif score >= 150:
        if current_level == 2:
            level_up_sound()
            current_level = 3
        fps.tick(30)


def snake_body_collision():
    for snake_part in snake_body[1:]:
        if snake_position == snake_part:
            game_over()
            time.sleep(2)


def snake_body_building():
    for index, part in enumerate(snake_body):
        if index == 0:
            screen.blit(rotated_head, (part[0], part[1]))
        else:
            pygame.draw.rect(screen, "green", pygame.Rect(part[0], part[1], 20, 20))


def steering_keys_setup():
    global change_dir
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_UP:
            change_dir = "UP"
        if event.key == pygame.K_DOWN:
            change_dir = "DOWN"
        if event.key == pygame.K_LEFT:
            change_dir = "LEFT"
        if event.key == pygame.K_RIGHT:
            change_dir = "RIGHT"


def steering_rules():
    global change_dir, direction
    if change_dir == "UP" and direction != "DOWN":
        direction = "UP"
    if change_dir == "DOWN" and direction != "UP":
        direction = "DOWN"
    if change_dir == "LEFT" and direction != "RIGHT":
        direction = "LEFT"
    if change_dir == "RIGHT" and direction != "LEFT":
        direction = "RIGHT"


def snake_movement():
    global rotated_head
    if direction == "RIGHT":
        snake_position[0] += 20
        rotated_head = pygame.transform.rotate(snake_head_img, 90)
    if direction == "LEFT":
        snake_position[0] -= 20
        rotated_head = pygame.transform.rotate(snake_head_img, -90)
    if direction == "UP":
        snake_position[1] -= 20
        rotated_head = pygame.transform.rotate(snake_head_img, 180)
    if direction == "DOWN":
        snake_position[1] += 20
        rotated_head = snake_head_img
    snake_body.insert(0, list(snake_position))


def apple_eating():
    global apple_position, snake_position, score, apple_spawn, snake_body
    if apple_position == snake_position:
        apple_snd.set_volume(0.4)
        apple_snd.play()
        score += 10
        apple_spawn = False
    else:
        snake_body.pop()
    if not apple_spawn:
        apple_position = [random.randrange(1, (screen_width // 20)) * 20,
                          random.randrange(1, ((screen_height - 100) // 20)) * 20]
        apple_spawn = True
    screen.blit(background_img, (0, 0))


while start:
    start_screen()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            start = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                run = True
                start = False
    pygame.display.flip()

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            steering_keys_setup()

        steering_rules()
        snake_movement()
        apple_eating()
        snake_body_building()
        screen.blit(apple_img, (apple_position[0], apple_position[1]))
        screen_boundaries()
        game_leveling_speed()
        snake_body_collision()
        pygame.draw.rect(screen, "black", (0, 640, screen_width, 40))
        show_score()
        pygame.display.update()

start_screen()
